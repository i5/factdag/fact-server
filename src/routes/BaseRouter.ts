import {NextFunction, Request, Response, Router} from 'express';
import * as STATUS from 'http-status-codes';
import MementoDao from '@daos/Memento/MementoDao';

import LdpMiddleware from '../middleware/LdpMiddleware';
import {
    createURIM,
    datetimeTotimestamp,
    fullUrl,
    serializeTimemap,
    timestampToDatetime,
    createRevisionID,
    removeTrailingSlash,
    trimWhiteSpaces
} from '@shared/functions';
import * as CONST from '@shared/constants';
import cors from "cors";
import BinaryMementoHandler from "@daos/Memento/BinaryMementoHandler";
import logger from '@shared/Logger';
const path = require('path')
// Init shared
const router = Router();
const mementoDao = new MementoDao();
const ldpMiddleWare = new LdpMiddleware();
const binaryHandler = new BinaryMementoHandler();

function hostString(req: Request): string {
    return req.protocol + '://' + req.get('host') + req.path;
}

async function isBinaryRequest(req: Request): Promise<boolean> {
    const resourceID = fullUrl(req);
    const ldpType = await mementoDao.getLDPType(resourceID);
    return ldpType === CONST.LDP_RESOURCE_TYPE.LDP_BINARY;
}

function setResponseHeaders(req: Request, res: Response) {
    res.set("Access-Control-Expose-Headers", "Memento-Datetime, Location, Link");
    res.set(CONST.HEADER.ALLOW, "GET,HEAD,OPTIONS,PUT,DELETE,POST");
    res.set("accept-post", "text/turtle,application/ld+json,application/n-triples,*/*");
}

router.options('*', async (req: Request, res: Response) => {
    setResponseHeaders(req, res);
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "GET,HEAD,PUT,PATCH,POST,DELETE");
    res.set("Access-Control-Allow-Headers", "cache-control, Slug, Content-Type, Link, Accept-Datetime");
    res.set(CONST.HEADER.VARY, "Access-Control-Request-Headers");
    res.status(STATUS.NO_CONTENT).send();
});

// Retrieve a Timemap
router.get('*', cors(), async (req: Request, res: Response, next: NextFunction) => {
    setResponseHeaders(req, res);
    res.append(CONST.HEADER.LINK, ['<http://www.w3.org/ns/ldp#Resource>; rel="type"']);
    if (req.method !== 'GET' || req.query.ext !== 'timemap') {
        next();
        return;
    }
    const page = req.query.page;
    const url = fullUrl(req);
    const range = req.header(CONST.HEADER.ACCEPT_MEMENTO_RANGE);
    const isBinary = await isBinaryRequest(req);
    let format = req.header(CONST.HEADER.ACCEPT);
    if (format === '*/*') {
        format = CONST.CONTENT_TYPE.APPLICATION_LINK_FORMAT;
    }
    if (format === CONST.CONTENT_TYPE.APLLICATION_JSON || format === CONST.CONTENT_TYPE.APPLICATION_LINK_FORMAT || format === CONST.CONTENT_TYPE.TEXT_TURTLE) {
        let timestamps, timemap;
        /*if (isBinary) {
            return binaryHandler.getTimeMap(req, res);
        }*/
        if (page && typeof page === 'string') {
            const pageInt = Number.parseInt(page);
            if(pageInt<0) {
                res.status(STATUS.BAD_REQUEST).send();
                return;
            }
            timestamps = await mementoDao.getTimemapByPage(url, pageInt);
            // Create Page Pointers
            let pages = {};
            const pagesize = Number.parseInt(process.env.MEMENTO_TIMEMAP_PAGESIZE || '100');
            if (pageInt != 0) {
                pages = {"prev": await mementoDao.getTimeMapPagePointer(url, pageInt-1, pagesize)};
            }
            const nextPage = await mementoDao.getTimeMapPagePointer(url, pageInt+1, pagesize);
            if (timestamps.length === pagesize && nextPage!==null) {
                pages = {...pages, ...{"next": nextPage}};
            }
            timemap = serializeTimemap(url, format, timestamps, pages);
        } else if (range !== undefined) {
            const rangesplit = range.split('/');
            timestamps = await mementoDao.getTimemapByRange(url, rangesplit[0], rangesplit[1]);
            timemap = serializeTimemap(url, format, timestamps);
        } else {
            timestamps = await mementoDao.getTimemap(url);
            timemap = serializeTimemap(url, format, timestamps);
        }

        res.set(CONST.HEADER.CONTENT_TYPE, format);
        res.status(STATUS.OK).send(timemap);
    } else {
        res.status(STATUS.NOT_ACCEPTABLE).send();
    }
});

// Directly retrieve a Memento
router.get('*', cors(), async (req: Request, res: Response, next: NextFunction) => {
    setResponseHeaders(req, res);
    const factid = req.query.version;
    if (req.method !== 'GET' || req.query.version === undefined || typeof req.query.version !== 'string') {
        next();
        return;
    }
    const acceptHeader = req.header(CONST.HEADER.ACCEPT);
    if (factid && typeof factid === 'string') {
        const isBinary = await isBinaryRequest(req);
        let memento;
        if (isBinary && acceptHeader!== CONST.CONTENT_TYPE.TEXT_TURTLE && req.query.ext !== 'description') {
            res.append(CONST.HEADER.LINK, ['<' + fullUrl(req)+'?version='+factid + '&ext=description>; rel="describedby"']);
            res.append(CONST.HEADER.LINK, ['<http://www.w3.org/ns/ldp#NonRDFSource>; rel="type"']);
            await binaryHandler.getBinary(req, res);
        } else {
            res.append(CONST.HEADER.LINK, ['<http://www.w3.org/ns/ldp#RDFSource>; rel="type"']);
            res.set(CONST.HEADER.CONTENT_TYPE, CONST.CONTENT_TYPE.TEXT_TURTLE);
            memento = await mementoDao.getOne(fullUrl(req), timestampToDatetime(factid));
            const resourceType = await mementoDao.getType(fullUrl(req));
            let resource = hostString(req);
            if (resourceType === null) {
                res.status(STATUS.NOT_FOUND).send();
            } else if (resourceType === CONST.RESOURCE_TYPE.MEMENTO_STACK) {
                resource = await mementoDao.getLocation(fullUrl((req)));
            }
            res.append(CONST.HEADER.LINK, ['<' + resource + '>; rel=original']);
            if (memento != null) {
                res.status(STATUS.OK).send(memento.content);
            } else {
                res.status(STATUS.NOT_FOUND).send();
            }
        }
    }
});

// Retrieve OR or Negotiate Datetime
router.get('*', cors(), async (req: Request, res: Response) => {
    setResponseHeaders(req, res);
    let memento;
    const mementoHeader = req.header(CONST.HEADER.ACCEPT_DATETIME);
    const acceptHeader = req.header(CONST.HEADER.ACCEPT);
    const resourceID = fullUrl(req);
    const ldpType = await mementoDao.getLDPType(resourceID);
    const isBinary = ldpType === CONST.LDP_RESOURCE_TYPE.LDP_BINARY;
    
    if (ldpType === CONST.LDP_RESOURCE_TYPE.LDP_CONTAINER) {
        res.append(CONST.HEADER.LINK, ['<http://www.w3.org/ns/ldp#BasicContainer>; rel="type"']);
        res.append(CONST.HEADER.LINK, ['<http://www.w3.org/ns/ldp#Container>; rel="type"']);
    }
    if (isBinary && acceptHeader!== CONST.CONTENT_TYPE.TEXT_TURTLE && req.query.ext !== 'description') {
        res.append(CONST.HEADER.LINK, ['<' + resourceID + '?ext=description>; rel="describedby"']);
        res.append(CONST.HEADER.LINK, ['<http://www.w3.org/ns/ldp#NonRDFSource>; rel="type"']);
        await binaryHandler.getBinary(req, res);
    } else {
        res.append(CONST.HEADER.LINK, ['<http://www.w3.org/ns/ldp#RDFSource>; rel="type"']);
        const resourceType = await mementoDao.getType(resourceID);
        if (resourceType === null) {
            res.status(STATUS.NOT_FOUND).send();
        }
        if (mementoHeader !== undefined) {
            memento = await mementoDao.getOne(resourceID, mementoHeader);
            if (memento) {
                const contentLocation = createURIM(resourceID, datetimeTotimestamp(memento.revisionID));
                res.header(CONST.HEADER.CONTENT_LOCATION, contentLocation);
                res.header(CONST.HEADER.VARY, CONST.HEADER.ACCEPT_DATETIME);
            }
        } else {
            if (resourceType === CONST.RESOURCE_TYPE.ORIGINAL_RESOURCE) {
                try {
                    memento = await mementoDao.getCurrent(resourceID);
                } catch (err) {
                    if (err === "DELETED") {
                        res.status(STATUS.GONE).send();
                        return;
                    } else {
                        throw err;
                    }
                }
            } else {
                memento = null;
            }
        }
        if (memento !== null){
            res.set(CONST.HEADER.MEMENTO_DATETIME, memento.revisionID);
            //res.set(CONST.HEADER.LOCATION, )
            res.set(CONST.HEADER.CONTENT_TYPE, CONST.CONTENT_TYPE.TEXT_TURTLE);
            if (resourceType === CONST.RESOURCE_TYPE.MEMENTO_STACK) {
                const originalLocation = await mementoDao.getLocation(resourceID);
                res.append(CONST.HEADER.LINK, ['<' + originalLocation + '>; rel=original']);
            } else {
                res.append(CONST.HEADER.LINK, ['<' + resourceID + '>; rel=original']);
            }
            res.status(STATUS.OK).send(memento.content);
        } else {
            res.status(STATUS.NOT_FOUND).send();
        }
    }
});

/**
 * Post a new Resource to an existing Container. If a slug is specified, the server will try to create the new resource under that URI.
 * If no slug is specified, the URI is chosen by the server.
 * If a resource was created sucessfully, 201 is returned.
 * If the url of the request is not an existing container, 404 is returned.
 */
router.post('*', cors(), async (req: Request, res: Response) => {
    setResponseHeaders(req, res);
    const datetime = req.header(CONST.HEADER.MEMENTO_DATETIME);
    const location = req.header(CONST.HEADER.CONTENT_LOCATION);
    const link = req.header(CONST.HEADER.LINK);
    let slug = req.header(CONST.HEADER.SLUG);
    const parentContainer = removeTrailingSlash(fullUrl(req))+'/'

    const isContainer = await mementoDao.getLDPType(parentContainer);
    if (!isContainer) {
        res.status(STATUS.NOT_FOUND).send();
        return;
    }

    let type;
    if (slug !== undefined) {
        type = await mementoDao.getLDPType(parentContainer+slug);
    } else { // server assigns slug
        slug = (Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5));
        type = await mementoDao.getLDPType(parentContainer+slug);
        while(type) {
            logger.info(`Slug "${slug}" is already in use. Reassigning...`);
            slug = (Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5));
            type = await mementoDao.getLDPType(parentContainer+slug);
        }
        logger.info(`Random slug assigned: ${parentContainer}${slug}`)
    }

    const isBinary = (trimWhiteSpaces(link) === '<http://www.w3.org/ns/ldp#NonRDFSource>;rel="type"') || (type == CONST.LDP_RESOURCE_TYPE.LDP_BINARY); 

    const id = createRevisionID();
    let memento;
    if (isBinary && req.header(CONST.HEADER.CONTENT_TYPE) !== CONST.CONTENT_TYPE.TEXT_TURTLE) { //new binary revision
        if (type) {
            await mementoDao.copyOR(parentContainer+slug,id); // copy existing description
        } else {
            await ldpMiddleWare.createLdpResource(parentContainer, "", link, slug, id); //create new description resource
        }
        return (await binaryHandler.writeBinary(req, res, id));
    } else if(isBinary && req.header(CONST.HEADER.CONTENT_TYPE) === CONST.CONTENT_TYPE.TEXT_TURTLE) { //new binary description resource
        await ldpMiddleWare.createLdpResource(parentContainer, req.body.toString(), '<http://www.w3.org/ns/ldp#NonRDFSource>; rel="type"', slug, id); //create new description resource
        return (await binaryHandler.copyBinary(req, res, id)); // copy existing binary
    } else {
        if (datetime && location) { // Create Memento-Resource without OR
            memento = await mementoDao.addToMementoStack(fullUrl(req), req.body.toString(), datetimeTotimestamp(datetime), location);
        } else { // Create OR
            try {
                memento = await ldpMiddleWare.createLdpResource(parentContainer, req.body.toString(), link, slug, id); // TODO: Stringification ideally only for dev purposes.
            }
            catch(err) {
                switch(err) {
                    case "BAD_LINK":
                        res.status(400).send();
                        break;
                    default:
                        throw err;
                }
                return;
            }
        }
        res.set(CONST.HEADER.MEMENTO_DATETIME, memento.revisionID);
        res.set(CONST.HEADER.LOCATION, createURIM(memento.resourceID, datetimeTotimestamp(memento.revisionID)));
        res.status(STATUS.CREATED).send();
    }
});

/**
 * PUTs content directly to a resource. If the resource exists, a new revision is created.
 * A new resource may be created using PUT if the ldp type is specified in the Link header.
 * A new resource crated with PUT may be disconnected from any containers.
 */
router.put('*', cors(), async (req: Request, res: Response) => {
    setResponseHeaders(req, res);
    const datetime = req.header(CONST.HEADER.MEMENTO_DATETIME);
    const location = req.header(CONST.HEADER.CONTENT_LOCATION); // TODO: Evaluate if this is the best header for that purpose.
    const link = req.header(CONST.HEADER.LINK);

    const type = await mementoDao.getLDPType(fullUrl(req));
    const isBinary = (trimWhiteSpaces(link) === '<http://www.w3.org/ns/ldp#NonRDFSource>;rel="type"') || (type == CONST.LDP_RESOURCE_TYPE.LDP_BINARY);

    const id = createRevisionID();
    let memento;
    if (isBinary && req.header(CONST.HEADER.CONTENT_TYPE) !== CONST.CONTENT_TYPE.TEXT_TURTLE) {
        if (type) {
            await mementoDao.copyOR(fullUrl(req), id);
        } else {
            await ldpMiddleWare.putLdpResource(fullUrl(req), "", link, id);
        }
        await binaryHandler.writeBinary(req,  res, id);
    } else if(isBinary && req.header(CONST.HEADER.CONTENT_TYPE) === CONST.CONTENT_TYPE.TEXT_TURTLE) { //new binary description resource
        await ldpMiddleWare.putLdpResource(fullUrl(req), req.body.toString(), '<http://www.w3.org/ns/ldp#NonRDFSource>; rel="type"', id); //create new description resource
        return (await binaryHandler.copyBinary(req, res, id)); // copy existing binary
    } else {
        if (datetime && location) { // Create Memento-Resource without OR
            memento = await mementoDao.addToMementoStack(fullUrl(req), req.body.toString(), datetimeTotimestamp(datetime), location);
        } else { // Create OR
            try {
                memento = await ldpMiddleWare.putLdpResource(fullUrl(req), req.body.toString(), link, id); // TODO: Stringification ideally only for dev purposes.
            } catch (e) {
                res.status(STATUS.CONFLICT).send();
                throw e; 
            }
        }
        res.set(CONST.HEADER.MEMENTO_DATETIME, memento.revisionID);
        res.set(CONST.HEADER.LOCATION, createURIM(memento.resourceID, datetimeTotimestamp(memento.revisionID)));
        res.status(STATUS.CREATED).send();
    }
});



router.delete('*', cors(), async (req: Request, res: Response) => {
    setResponseHeaders(req, res);
    await mementoDao.delete(fullUrl(req));
    res.status(STATUS.NO_CONTENT).send();
});

/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
