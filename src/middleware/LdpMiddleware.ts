
import MementoDao from '@daos/Memento/MementoDao';
import { LDP_RESOURCE_TYPE } from '@shared/constants';
import { removeTrailingSlash, trimWhiteSpaces, createRevisionID } from '@shared/functions';
import logger from '@shared/Logger';
const path = require('path');
const mementoDao = new MementoDao();

class LdpMiddleware {
    
    constructor() {
        this.createRoot();
     }
    
    /**
     * Creates a Root container on Server start. 
     */
    private async createRoot(): Promise<void> {
        const root = process.env.ROOT || "http://localhost:3000/";
        const exists = await mementoDao.getType(root);
        if (!exists) {
            logger.info(`Creating Root Container at ${root}`);
            this.createLdpResource(root,"",'<http://www.w3.org/ns/ldp#BasicContainer>; rel="type"',"",createRevisionID());
        }
    }

    /**
     * This function handles resource creation on LDP level, if a resource is posted to a container. 
     * @param resourceID The URI of the container that the new resource is being posted to.
     * @param content The RDF payload of the new resource.
     * @param link The link header specifying what kind of LDP resource is being posted. If none is specified,
     * the function will check wether a version already exists at this adress and assign the corresponding type.
     * If no link header is given and no existing resource at the adress is found, a RDFSource will be created.
     * @param slug The resource name. 
     */
    public async createLdpResource(resourceID: string, content: string, link: any, slug: any, revisionID: string) {
        resourceID = removeTrailingSlash(resourceID)+'/';
        let type: LDP_RESOURCE_TYPE | undefined;
        if(link) {
            link = trimWhiteSpaces(link);
        }
        // This switch block checks wether a link header is specified for the new resource and sets the type accordingly
        switch(link) { 
            case '<http://www.w3.org/ns/ldp#BasicContainer>;rel="type"': // LDP Basic Container
                if (slug !== "") {
                    slug += '/';
                }
                type = LDP_RESOURCE_TYPE.LDP_CONTAINER;
                content = this.addTypeTriple(resourceID+slug,type,"");
                break;
            case '<http://www.w3.org/ns/ldp#RDFSource>;rel="type"': // LDP RDF Source
                type = LDP_RESOURCE_TYPE.LDP_RDFSource;
                content = this.addTypeTriple(resourceID+slug,type,content);
                break;
            case '<http://www.w3.org/ns/ldp#NonRDFSource>;rel="type"': // LDP Non-RDF Source
                type = LDP_RESOURCE_TYPE.LDP_BINARY;
                content = this.addTypeTriple(resourceID+slug,type,content);
                break;
            case undefined: // If no link-header was specified, the function checks if an older revision exists
                type = await mementoDao.getLDPType(removeTrailingSlash(resourceID));
        
                if (type === LDP_RESOURCE_TYPE.LDP_BINARY) {
                    resourceID = removeTrailingSlash(resourceID);
                    content = this.addTypeTriple(resourceID,type,content);
                } else if (type === LDP_RESOURCE_TYPE.LDP_CONTAINER) {
                    content = this.addTypeTriple(resourceID,type,content);
                } else { // if the type is rdfsource or undefined
                    resourceID = removeTrailingSlash(resourceID);
                    content = this.addTypeTriple(resourceID,LDP_RESOURCE_TYPE.LDP_RDFSource,content);
                }

                if (!slug) {
                    content = this.completeLocationPrefixes(resourceID, content, slug);
                    content = this.completeEmptyTriples(resourceID, content, slug)
                    return await mementoDao.addToOR(resourceID, content, revisionID, type);
                }
                resourceID = removeTrailingSlash(resourceID)+'/';
                break;
            default: // An unknown or unsupported ldp type is specified in the link header
                throw "BAD_LINK"

        }
        content = this.completeLocationPrefixes(resourceID, content, slug);
        content = this.completeEmptyTriples(resourceID, content, slug)
        return await mementoDao.addToOR(resourceID+slug, content, revisionID, type);
    }

    /**
     * This function handles PUT requests to resources on LDP level. 
     * @param resourceID - The URI of the resource that is updated.
     * @param content - The RDF payload of the resource.
     * @param link - The link header specifying the kind of LDP resource, if a new resource is being PUT. 
     */
    public async putLdpResource(resourceID: string, content: string, link: any, revisionID: string) {
        if(content.includes('ldp:contains') || content.includes('<http://www.w3.org/ns/ldp#contains>')) {
            throw "CONFLICT"
        }
        if(link) {
            link = trimWhiteSpaces(link);
        }
        resourceID = removeTrailingSlash(resourceID);
        let type: LDP_RESOURCE_TYPE | undefined;
        // This switch block checks wether a link header is specified for the new resource and sets the type accordingly
        switch(link) {
            case '<http://www.w3.org/ns/ldp#BasicContainer>;rel="type"': // LDP Basic Container
                resourceID+='/';
                type = LDP_RESOURCE_TYPE.LDP_CONTAINER;
                content = this.addTypeTriple(resourceID,type,"");
                break;
            case '<http://www.w3.org/ns/ldp#RDFSource>;rel="type"': // LDP RDF Source
                type = LDP_RESOURCE_TYPE.LDP_RDFSource;
                content = this.addTypeTriple(resourceID,type,content);
                break;
            case '<http://www.w3.org/ns/ldp#NonRDFSource>;rel="type"': // LDP Non-RDF Source
                type = LDP_RESOURCE_TYPE.LDP_BINARY;
                content = this.addTypeTriple(resourceID,type,content);
                break;
            case undefined: // If no link-header was specified, the function checks if an older revision exists
                type = await mementoDao.getLDPType(removeTrailingSlash(resourceID));
                if (type === LDP_RESOURCE_TYPE.LDP_BINARY) {
                    content = this.addTypeTriple(removeTrailingSlash(resourceID),type,content);
                } else if (type === LDP_RESOURCE_TYPE.LDP_CONTAINER) {
                    resourceID+='/';
                    content = this.addTypeTriple(resourceID,type,content);
                } else {
                    content = this.addTypeTriple(removeTrailingSlash(resourceID),LDP_RESOURCE_TYPE.LDP_RDFSource,content);
                }

                content = this.completeLocationPrefixes(resourceID, content);

                return await mementoDao.addToOR(resourceID, content, revisionID, type);
            default: // An unknown or unsupported ldp type is specified in the link header
                throw "BAD_LINK"
        }
        content = this.completeLocationPrefixes(resourceID, content);
        return await mementoDao.addToOR(resourceID, content, revisionID, type);
    }

    /**
     * Adds the corresponding ldp type triples to the payload.
     * @param resourceID - The URI of the resource
     * @param type - The LDP Type
     * @param content - The payload
     */
    private addTypeTriple(resourceID: string, type: LDP_RESOURCE_TYPE, content: string) {
        let ldpType: string;
        switch(type) {
            case 'LDP_CONTAINER':
                ldpType = 'ldp:BasicContainer';
                break;
            case 'LDP_BINARY':
                ldpType = 'ldp:NonRDFSource';
                break;
            default:
                ldpType = 'ldp:RDFSource'
        }
        if (content) {
            content += '@prefix ldp:<http://www.w3.org/ns/ldp#> . <'+resourceID+'> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> '+ldpType+' .';
        } else {
            content = '@prefix ldp:<http://www.w3.org/ns/ldp#> . <'+resourceID+'> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> '+ldpType+' .';
        }
        return content;
    }

    /**
     * This function replaces any badly defined prefixes so FUSEKI does not attempt to auto-complete them.
     * This is mostly relevant for factlibs location prefixes. 
     */
    private completeLocationPrefixes(resourceID: string, content: string, slug?: string) {
        let location
        if(slug) {
            location = resourceID;
        }
        else {
            location = resourceID.replace(path.basename(resourceID)+'/',"");
        }
        const regExp = /@prefix(.*)?:\s*<((?!.*http:\/\/).*)>\s*?\./g;
        let match = regExp.exec(content);
        while (match != null) {
            content = content.replace(match[0],'@prefix '+match[1]+': <'+new URL(match[2],location).toString()+'>');
            match = regExp.exec(content);
        }
        return content;
    }

    /**
     * This function fills in blank triple subjects/objects with the resource URI.
     * This is mostly used when clients do not specify a resource name for creation but want to refrence the resource URI in the payload. 
     */
    private completeEmptyTriples(resourceID: string, content: string, slug:string) {
        const regExp = /<((?!http:\/\/).*?)>/g;
        let match = regExp.exec(content);
        while (match != null) {
            content = content.replace(match[0],'<'+new URL(match[1] || slug , resourceID).toString()+'>');
            match = regExp.exec(content);
        }
        return content;
    }

}

export default LdpMiddleware;
