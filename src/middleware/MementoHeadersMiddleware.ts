import { Request, Response, NextFunction } from 'express';

const handler = (req: Request, res: Response, next: NextFunction) => {
    if(req.method !== 'GET' && req.method !== 'HEAD') {
        next();
        return;
    }

    const resource = req.protocol + '://' + req.get('host') + req.path;
    const timemapFragment = '?ext=timemap';
    res.append('Link', ['<' + resource + '>; rel="timegate"', '<' + resource + timemapFragment + '>; rel="timemap"']);

    next();
};

/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default handler;