import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import helmet from 'helmet';

import express, {Request, Response, NextFunction} from 'express';
import { BAD_REQUEST } from 'http-status-codes';
import 'express-async-errors';

import logger from '@shared/Logger';
import BaseRouter from "./routes/BaseRouter";
import MementoHeadersMiddleware from "./middleware/MementoHeadersMiddleware";
// @ts-ignore
import preconditions from "express-preconditions";

// Init express

const app = express();
/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(preconditions({requiredWith:[]}));
app.set('etag', 'strong')

app.use(express.raw({type: '*/*', limit: '50mb' })); // TODO Consider Limitations
app.use(cookieParser());

// Show routes called in console during development
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// Security
if (process.env.NODE_ENV === 'production') {
    app.use(helmet());
}

// Print API errors
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
    logger.error(err.message, err);
    return res.status(BAD_REQUEST).json({
        error: err.message,
    });
});

/************************************************************************************
 *                              Serve front-end content
 ***********************************************************************************/

app.use(MementoHeadersMiddleware);
app.use(BaseRouter);

// Export express instance
export default app;
