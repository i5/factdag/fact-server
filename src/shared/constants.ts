
export enum RESOURCE_TYPE {
    MEMENTO_STACK = 'MEMENTO_STACK',
    ORIGINAL_RESOURCE = 'ORIGINAL_RESOURCE'
}

export enum CONTENT_TYPE {
    APLLICATION_JSON = 'application/json',
    APPLICATION_LINK_FORMAT = 'application/link-format',
    TEXT_TURTLE = 'text/turtle'
}

export enum HEADER {
    ACCEPT = 'Accept',
    ALLOW = 'Allow',
    ACCEPT_DATETIME = 'Accept-Datetime',
    MEMENTO_DATETIME = 'Memento-Datetime',
    CONTENT_TYPE = 'Content-Type',
    ACCEPT_MEMENTO_RANGE = 'Accept-Memento-Range',
    LOCATION = 'Location',
    CONTENT_LENGTH = 'Content-Length',
    SLUG = 'Slug',
    CONTENT_LOCATION = 'Content-Location',
    VARY = 'Vary',
    LINK = 'Link',

}

export enum LDP_RESOURCE_TYPE {
    LDP_CONTAINER = "LDP_CONTAINER",
    LDP_RDFSource = "LDP_RDFSource",
    LDP_BINARY = "LDP_BINARY"
}

