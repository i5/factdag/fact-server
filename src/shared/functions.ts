import {Request} from "express";
import Url, {UrlObject} from "url";
import * as CONST from '@shared/constants';

export const fullUrlObj = (req: Request): UrlObject => {
    return {
        protocol: req.protocol,
        host: req.get('host'),
        pathname: Url.parse(req.url).pathname,
    };
};

export const fullUrl = (req: Request): string => {
    return Url.format(fullUrlObj(req));
};

export const timestampToDatetime = (timestamp: string): string => {
    const splittime = timestamp.split("-");
    const time = Number(splittime[0]);
    let countstring =  splittime [1];
    const places = Number.parseInt(process.env.SUB_MILLI_PLACES || "3");
    if (places !== -1) {
        const maxval = Math.pow(10, places) - 1;
        if (Number.parseInt(countstring) > maxval ) {
            countstring =  maxval.toString();
        }
        while (countstring.length < places) {
            countstring = "0" + countstring;
        }
    }

    const date = new Date(time);
    const datestring = date.toISOString();
    return (datestring.substring(0, datestring.length-1) + countstring + "Z");
};

export const datetimeTotimestamp = (datetime: string): string => {
    const milli = Date.parse(datetime).toString();
    const lastpoint = datetime.lastIndexOf(".");
    const counter = datetime.substring(lastpoint+4, datetime.length-1);
    //return milli + "-" + parseInt(counter);
    return milli + "-0";
};

export const createURIM = (resourceID: string, timestamp: string): string => {
    return  resourceID + "?version=" + timestamp;
};

export const assembleLinkFormatTimeMap = (resourceID: string, timestamps: any): string => {
    let result = "";
    result += "<" + resourceID + ">;rel=\"timegate original\",\n"; // Link to TimeGate and URI-O
    result += "<" + resourceID + "?ext=timemap>; rel=\"timemap\"; type=\"application/link-format\",\n"; //Link to TimeMap
    timestamps.forEach((value: string) => {
        result += "<" + createURIM(resourceID,value) + ">" +
            "; rel=\"memento\";datetime=\"" + timestampToDatetime(value) + "\",\n";
    });
    return result.substring(0, result.length-2);
};

export const assembleJsonTimeMap = (resourceID: string, timestamps: any, pages?: Object): string => {
    const mementos: { datetime: string; uri: string }[] = []
    timestamps.forEach((value: string) => {
        const memento = {
            "datetime" : timestampToDatetime(value),
            "uri" : createURIM(resourceID,value)
        };
        mementos.push(memento);
    });
    let dateObject = {
        "original_uri": resourceID,
        "timemap_uri" : resourceID + "?ext=timemap",
        "mementos": mementos,
    };
    if (pages) {
        dateObject = {...dateObject, ...{"pages": pages}};
    }
    return JSON.stringify(dateObject);
};

export const assembleTurtleTimeMap = (resourceID: string, timestamps: any): string => {
    let result = "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n";
    result += "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n";
    result += "<" + resourceID + ">\n";
    result += "        rdf:type  <http://mementoweb.org/ns#OriginalResource> ;\n";
    result += "        rdf:type  <http://mementoweb.org/ns#TimeGate> ;\n";
    result += "        <http://mementoweb.org/ns#timegate>" + " <" + resourceID + "> ;\n";
    result += "        <http://mementoweb.org/ns#timemap>" +  " <" + resourceID + "?ext=timemap> ;\n";

    timestamps.forEach((value: string) => {
        result += "        <http://mementoweb.org/ns#memento>" +  " <" + createURIM(resourceID,value) + "> ;\n";
    });
    result = result.slice(0, -2) + '.\n\n';

    result += "<" + resourceID + "?ext=timemap>\n";
    result += "        rdf:type  <http://mementoweb.org/ns#TimeMap> ;\n";
    result += "        rdf:type  <http://mementoweb.org/ns#TimeMap> .\n\n";
    // result += "        <http://www.w3.org/2006/time#hasBeginning> " + " <http://reference.data.gov.uk/id/gregorian-instant/" + datetimeBeginning + "> ;\n";
    // result += "        <http://www.w3.org/2006/time#hasEnd> " + " <http://reference.data.gov.uk/id/gregorian-instant/" + datetimeEnd + "> .\n\n";

    timestamps.forEach((value: string) => {
        result += "<" + createURIM(resourceID, value) + ">\n";
        result += "        rdf:type  <http://mementoweb.org/ns#Memento> ;\n";
        result += "        <http://mementoweb.org/ns#original>  <" + resourceID + "> ;\n";
        result += "        <http://mementoweb.org/ns#timegate>  <" + resourceID + "> ;\n";
        result += "        <http://mementoweb.org/ns#timemap>  <" + resourceID + "?ext=timemap> ;\n";
        result += "        <http://www.w3.org/2006/time#hasTime>  <http://reference.data.gov.uk/id/gregorian-instant/" + timestampToDatetime(value) +"> ;\n";
        result += "        <http://mementoweb.org/ns#mementoDatetime> \"" + timestampToDatetime(value) +"\"^^xsd:dateTime.\n\n";
    });
    return result;
};

export const serializeTimemap = (resourceID: string, format: string, timestamps: any, pages?: Object): string => {
    let serialization;
    if (format === CONST.CONTENT_TYPE.APLLICATION_JSON) {
        serialization = assembleJsonTimeMap(resourceID, timestamps, pages);
    } else if (format === CONST.CONTENT_TYPE.TEXT_TURTLE) {
        serialization = assembleTurtleTimeMap(resourceID, timestamps);
    } else {
        serialization = assembleLinkFormatTimeMap(resourceID, timestamps);
    }
    return serialization;
};

export const isDatetimeInFuture = (mementoHeader: string): boolean => {
    const requestTime = Date.parse(mementoHeader);
    const now = Date.now();
    return (now < requestTime);
}

export const trimWhiteSpaces = (s : string|undefined): string => {
    if(s) {
        return s.replace(/\s/g, "");
    }
    else {
        return "";
    }
}

export const removeTrailingSlash = (s :string): string => {
    return s.replace(/\/$/, "");
}

export const createRevisionID = (): string => {
    return Date.now().valueOf().toString();
}
