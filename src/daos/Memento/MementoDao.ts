import axios from 'axios';
import Memento, {IMemento} from '@entities/Memento';
import {createRevisionID, datetimeTotimestamp, removeTrailingSlash, timestampToDatetime} from "@shared/functions";
import {RESOURCE_TYPE, LDP_RESOURCE_TYPE} from "@shared/constants";

export interface IMementoDao {
    getOne: (resourceID: string, revisionID: string) => Promise<IMemento | null>;
    getAll: (resourceID: string) => Promise<IMemento[]>;
    addToOR: (resourceID: string, content: string, revisionID: string) => Promise<IMemento>;
    delete: (resourceID: string) => Promise<void>;
}

class MementoDao implements IMementoDao {

    public fusekiUrl = process.env.FUSEKIENDPOINT;

    /**
     * Gets a memento of a resource, that is closest to the given @param revisionID.
     */
    public async getOne(resourceID: string, revisionID: string): Promise<IMemento | null> {
        const isDirectory = await this.getLDPType(resourceID)===LDP_RESOURCE_TYPE.LDP_CONTAINER;
        if (isDirectory) {
            resourceID = removeTrailingSlash(resourceID)+'/';
        }

        try {
            let revision = await this.getClosestRevision(resourceID,revisionID);
            const id = revision[0]
            const content = revision[1]
            return this.createMementoFromResult(id+'-0', content, resourceID);
        } catch (err) {
            return null;
        }
    }

    /**
     * Gets a memento of the last version of a resource. 
     * For Containers, the containment relations get injected live. 
     */
    public async getCurrent(resourceID: string): Promise<IMemento | null> { 
        const isDirectory = await this.getLDPType(resourceID)===LDP_RESOURCE_TYPE.LDP_CONTAINER;
        if (isDirectory) {
            resourceID = removeTrailingSlash(resourceID)+'/';
        }

        try {          
            let lastRevision = await this.getCurrentRevision(resourceID);
            const id = lastRevision[0]
            let content = lastRevision[1]

            if (content.match(/<http:\/\/example.org\/resource>\s*<http:\/\/example.org\/is>\s*"DELETED"/) 
            || content.match(/ex:resource\s*ex:is\s*"DELETED"/)) { //ToDo: include in query
                throw "DELETED";
            }

            if (isDirectory) { //This block injects the containment triples for containers
                const children = await this.getContainedResources(resourceID);
                if (children.length > 0) {
                    content += '<'+resourceID+'> <http://www.w3.org/ns/ldp#contains> '+children.join()+'.';
                }
            }
            
            return this.createMementoFromResult(id+'-0', content, resourceID);
        } catch (err) {
            throw err;
        }
    }


    /**
     *
     */
    public async getAll(resourceID: string): Promise<IMemento[]> {
        // TODO
        // client.xrange(req.path, "-", "+", function(err, val) {
        return [] as any;
    }

    /**
     * Returns a Timemap in page form. A page contains a fixed amount of entries and points to the previous and next timemap page. 
     * @param resourceID - The URI of which to retrieve the timemap
     * @param page - The page number
     */
    public async getTimemapByPage(resourceID: string, page: number): Promise<any> {
        const pagesize = Number.parseInt(process.env.MEMENTO_TIMEMAP_PAGESIZE || '100');
        const from = page*pagesize;
        const query =
            `PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT ?time {
                FILTER(strStarts(str(?g),"${resourceID}?version="))
                GRAPH ?g{}
                BIND(strafter(str(?g),"?version=") AS ?time)
            } ORDER BY asc(?time)
            OFFSET ${from} 
            LIMIT ${pagesize}`;
        const queryResult = await this.sparqlQuery(query);
        const mementoTimestamps: string[] = queryResult.data.results.bindings.map((x: any)=>x.time.value+'-0');
        return mementoTimestamps;
    }

    public async getTimeMapPagePointer(resourceID: string, page: number, pagesize: number) {
        const query =
            `PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT ?time {
                FILTER(strStarts(str(?g),"${resourceID}?version="))
                GRAPH ?g{}
                BIND(strafter(str(?g),"?version=") AS ?time)
            } ORDER BY asc(?time)
            OFFSET ${page*pagesize} 
            LIMIT ${pagesize}`;

        const queryResult = await this.sparqlQuery(query);
        const mementoTimestamps: string[] = queryResult.data.results.bindings.map((x: any)=>x.time.value+'-0');
        if(mementoTimestamps[0]) {
            const fromTime = mementoTimestamps[0];
            const toTime = mementoTimestamps[mementoTimestamps.length-1];
            return {
                "from": timestampToDatetime(fromTime),
                "until": timestampToDatetime(toTime),
                "uri": resourceID + "?ext=timemap&page="+(page)
            };
        }
        else{
            return null;
        } 
    }

    /**
     * 
     * @param resourceID - The URI of which to retrieve the timemap
     * @returns A timemap of entries within the specified range 
     */
    public async getTimemapByRange(resourceID: string, start_range: string, end_range: string): Promise<any> {
        const query =
            `PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT ?time {
                FILTER(strStarts(str(?g),"${resourceID}?version="))
                GRAPH ?g{}
                BIND(strafter(str(?g),"?version=") AS ?time)
                FILTER((xsd:integer(?time) >= ${datetimeTotimestamp(start_range)}) && (xsd:integer(?time) <= ${datetimeTotimestamp(end_range)}))
            } ORDER BY asc(?time)`;
        const result = (await this.sparqlQuery(query)).data.results.bindings.map((x: any)=>x.time.value+'-0');
        return result;
    }

    /**
     * 
     * @param resourceID - The URI of which to retrieve the timemap
     * @returns The resources timemap
     */
    public async getTimemap(resourceID: string): Promise<any> {
        const query =
            `SELECT ?time {
                FILTER(strStarts(str(?g),"${resourceID}?version="))
                GRAPH ?g{}
                BIND(strafter(str(?g),"?version=") AS ?time)
            } ORDER BY asc(?time)`;
        const result = (await this.sparqlQuery(query)).data.results.bindings.map((x: any)=>x.time.value+'-0');
        return result;
    }

    public async getType(resourceID: string): Promise<RESOURCE_TYPE | null> {
        if (resourceID!==process.env.ROOT) {
            resourceID = removeTrailingSlash(resourceID);
        }
        const query =
            `SELECT ?type {
                <${resourceID}> a ?type
            }`;
        const queryResult = await this.sparqlQuery(query);
        if (queryResult.data.results.bindings[0]) {
            return queryResult.data.results.bindings[0].type.value;
        } else {
            return null;
        }
    }

    public async getLDPType(resourceID: string): Promise<LDP_RESOURCE_TYPE | undefined> {
        if (resourceID!==process.env.ROOT) {
            resourceID = removeTrailingSlash(resourceID);
        }
        const query =
            `SELECT ?type {
                <${resourceID}>  <http://example.org/type>  ?type
            }`;
        const queryResult = await this.sparqlQuery(query);
        if (queryResult.data.results.bindings[0]) {
            return queryResult.data.results.bindings[0].type.value;
        } else {
            return undefined;
        }
    }

    public async getLocation(resourceID: string): Promise<string> {
        const query =
            `SELECT ?location {
                <${resourceID}> <http://example.org/location> ?location
            }`;
        const queryResult = await this.sparqlQuery(query);
        return queryResult.data.results.bindings[0].location.value;
    }

    private async add(resourceID: string, content: string, revID: string,id = '*'): Promise<IMemento> {
        return new Memento(resourceID, timestampToDatetime(revID+'-0'), content); //hacky way of adding a counter. TODO: find new way to assign IDs for resources that share an ID
    }


    /**
     * Adds a new revision of an Original Resource. 
     * @param resourceID - The URI of the OR
     * @param content - The payload of the new revision
     * @param ldptype - The type of LDP resource
     */
    public async addToOR(resourceID: string, content: string, revisionID: string, ldptype: LDP_RESOURCE_TYPE = LDP_RESOURCE_TYPE.LDP_RDFSource): Promise<IMemento> {
        if (resourceID===process.env.ROOT) { //special case: root container keeps the trailing slash 
            await this.sparqlPost("default", '<'+resourceID+'> a "'+RESOURCE_TYPE.ORIGINAL_RESOURCE+'"; <http://example.org/type> "'+LDP_RESOURCE_TYPE.LDP_CONTAINER+'".');
        }
        else if (ldptype === LDP_RESOURCE_TYPE.LDP_CONTAINER) { //Adds the type Original Resource and the LDP type of the resource to the default graph
            await this.sparqlPost("default", '<'+removeTrailingSlash(resourceID)+'> a "'+RESOURCE_TYPE.ORIGINAL_RESOURCE+'"; <http://example.org/type> "'+ldptype+'".');
        }
        else { //Adds the type Original Resource and the LDP type of the resource to the default graph
            await this.sparqlPost("default", '<'+resourceID+'> a "'+RESOURCE_TYPE.ORIGINAL_RESOURCE+'"; <http://example.org/type> "'+ldptype+'".');
        }
        //const id = createRevisionID();
        await this.sparqlPut(resourceID+'?version='+revisionID,content);
        return this.add(resourceID, content, revisionID);
    }

    /**
     * Creates a new revision of OR with the same RDF content.
     * This is used when a binary resource gets updated and its description stays the same. 
     */
    public async copyOR(resourceID: string, revisionID: string): Promise<IMemento> {
        const lastRevision = await this.getCurrentRevision(resourceID);
        const content = lastRevision[1]
        //const id = createRevisionID();
        await this.sparqlPut(resourceID+'?version='+revisionID,content);
        return this.add(resourceID, content, revisionID);
    }

    /**
     * Adds a new memento to a Memento Stack
     */
    public async addToMementoStack(resourceID: string, content: string, id: string, orLocation: string): Promise<IMemento> {
        await this.sparqlPost("default", '<'+resourceID+'> a "'+RESOURCE_TYPE.MEMENTO_STACK+'".');
        await this.sparqlPost("default", '<'+resourceID+'> <http://example.org/location> "'+orLocation+'".');
        return this.add(resourceID, content, id);
    }

    /**
     * Creates a new revision of the resource and sets a tombstone to mark it as deleted. 
     */
    public async delete(resourceID: string): Promise<void> {
        const id = createRevisionID(); 
        await this.sparqlPut(resourceID+'?version='+id,'@prefix ex: <http://example.org/> . ex:resource ex:is "DELETED" .'); 
        return;
    }

    // TODO: For Server operations, if possible avoid parsing the data into a Memento object.
    private createMementoFromResult(id: string, content: any, resourceID: string): IMemento | null {
        const timestamp = id;
        return new Memento(resourceID, timestampToDatetime(timestamp), content);
    }

    // Sparql helper functions

    /**
     * 
     * @param resourceID 
     * @returns An arry [id, content] for the current revision of the resource
     */
    public async getCurrentRevision(resourceID: string) :Promise<[any,string]>{
        const contentQuery = 
        `CONSTRUCT{?s ?p ?o. <ex:timestamp> <ex:timestamp> ?time. } {
             {
               SELECT DISTINCT ?g {
                 FILTER(strStarts(str(?g),"${resourceID}?version="))
                 GRAPH ?g {}
               }
               ORDER BY desc(?g)
               LIMIT 1
             }
             GRAPH ?g {?s ?p ?o
               
             }
             BIND(strafter(str(?g),"?version=") AS ?time)
           }`;
        // Execute the query and extract id and content from the returned graph.
        let lastRevision = (await this.sparqlQuery(contentQuery,'turtle')).data;
        const regex = /<ex:timestamp>\s+<ex:timestamp>\s+"(\d+)"\s*.\s*\n/
        let match = regex.exec(lastRevision)
        let id;
        if(match!=null) {
            id=match[1];
        }
        let content = lastRevision.replace(regex,"")
        return [id,content]
    }

    /**
     * 
     * @param resourceID 
     * @param revisionID 
     * @returns the closest revision of a resource to the given timestamp. Ignores deleted versions.
     */
    public async getClosestRevision(resourceID: string, revisionID:string) {
        const query = 
        `CONSTRUCT{?s ?p ?o. <ex:timestamp> <ex:timestamp> ?time}
        {
          {
            SELECT DISTINCT ?g ?time{
              FILTER(strStarts(str(?g),"${resourceID}?version="))
              GRAPH ?g{
                ?s ?p ?o
                FILTER(NOT EXISTS {<http://example.org/resource> <http://example.org/is> "DELETED"})
              }
              BIND(strafter(str(?g),"?version=") AS ?time)
            }
            ORDER BY asc(abs(<http://www.w3.org/2001/XMLSchema#integer>(?time) - ${datetimeTotimestamp(revisionID)}))
            LIMIT 1
          }
          GRAPH ?g {?s ?p ?o}
        }`;
        // Execute the query and extract id and content from the returned graph.
        let lastRevision = (await this.sparqlQuery(query,'turtle')).data;
        const regex = /<ex:timestamp>\s+<ex:timestamp>\s+"(\d+)"\s*.\s*\n/
        let match = regex.exec(lastRevision)
        let id;
        if(match!=null) {
            id=match[1];
        }
        let content = lastRevision.replace(regex,"")
        return [id,content]
    }

    /**
     * 
     * @param resourceID 
     * @returns an array of the containers containment relations. 
     */
    public async getContainedResources(resourceID: string) {
        return (await this.sparqlQuery(`
        SELECT ?name {
            FILTER (regex(str(?name), '${resourceID}.+') && !regex(str(?name), '${resourceID}.+/.+'))
            {
              SELECT DISTINCT ?name (max(?graph) as ?latest){
              GRAPH ?graph {}
              BIND(strbefore(str(?graph),"?version=") AS ?name)
              }
              GROUP BY ?name
            }
            GRAPH ?latest {
              FILTER(NOT EXISTS {<http://example.org/resource> <http://example.org/is> "DELETED"})
            }
          } `)).data.results.bindings.map((x: any)=>'<'+x.name.value+'>');
    }

    // Sparql Protocol functions

    public async sparqlQuery(query: string, format: string = 'json') { 
        const queryEncoded = encodeURIComponent(query);
        try {
            return await axios.get(this.fusekiUrl+'?query='+queryEncoded+'&format='+format);
        } catch (err) {
            throw err;
        }
    }

    // Prototype Sparql GSP functions 

    public async sparqlGet(graph: string) {
        try {
            return await axios.get(this.fusekiUrl+'/get?graph='+graph);
        } catch (err) {
            throw err;
        }
    }

    public async sparqlPut(graph: string, payload: string) {
        try {
            await axios({
                method: 'PUT',
                url: this.fusekiUrl+'/data?graph='+graph,
                headers: { 'content-type': 'text/turtle' },
                data: payload,
              });
        } catch (err) {
            throw err;
        }
    }

    public async sparqlPost(graph: string, payload: string) {
        try {
            await axios({
                method: 'POST',
                url: this.fusekiUrl+'/data?graph='+graph,
                headers: { 'content-type': 'text/turtle' },
                data: payload,
              });
        } catch (err) {
            throw err;
        }
    }

    public async sparqlDelete(graph: string) {
        try {
            await axios.delete(this.fusekiUrl+'?graph='+graph);
        } catch (err) {
            throw err;
        }
    }
}

export default MementoDao;
