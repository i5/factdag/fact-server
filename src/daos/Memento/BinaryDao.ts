import * as Minio from 'minio';
import {Readable, Stream} from "stream";
import {ItemBucketMetadata} from "minio";
import {OutgoingHttpHeaders} from "http";

/**
 * Combines the data stream of an object from the S3 store with its associated metadata required for the HTTP response.
 */
interface ObjectFromStore {
    createStream(): Promise<Stream>;
    headers(): Promise<OutgoingHttpHeaders>;
}

/**
 * Handles I/O for binary resources by reading and writing to a S3 store.
 */
class BinaryDao {
    private minioClient = new Minio.Client({
        endPoint: process.env.S3_ENDPOINT || "localhost",
        port: +(process.env.S3_PORT || 9000),
        useSSL: false,
        accessKey: process.env.S3_ACCESS_KEY || "minio",
        secretKey: process.env.S3_SECRET_KEY || "minio123"
    });

    /**
     * This I/O client works inside a single S3 bucket. The bucket is created at startup, if it does not yet exist.
     * @param bucketName - Name of the bucket to be used for this client.
     */
    constructor(private bucketName: string) {
       this.createBucket(bucketName);
    }

    /**
     * Creates a bucket if it does not yet exist.
     * @param bucketName - Name to the bucket to be created.
     */
    private async createBucket(bucketName: string): Promise<void> {
        const exists = await this.minioClient.bucketExists(bucketName.toLowerCase());
        if (!exists) {
            await this.minioClient.makeBucket(bucketName.toLowerCase(), '');
        }
    }

    /**
     * Takes a stream and puts it in the S3 bucket as a new object.
     * @param fstream - The data of the new object as a stream.
     * @param objName - The desired name of the S3 Object.
     * @param contentType - The mime type of object, stored as metadata with the object.
     * @param contentLength - The content length of the stream, to correctly read the data.
     */
    public async writeBinary(fstream: NodeJS.ReadableStream, objName: string, contentType: string, contentLength: string): Promise<string> {
        const metaData: ItemBucketMetadata = {
            'Content-Type': contentType,
        };
        return this.minioClient.putObject(this.bucketName, objName, Readable.from(fstream), parseInt(contentLength?contentLength:"0"), metaData);
    }

    /**
     * Copies an object and puts it in the S3 bucket as a new object.
     * @param objName - The desired name of the S3 Object.
     * @param srcObj - The name of the source Object.
     */
     public async copyBinary(objName: string, srcObj: string): Promise<Minio.BucketItemCopy> {
        var conds = new Minio.CopyConditions()
        const srcPath = this.bucketName+'/'+srcObj
        return this.minioClient.copyObject(this.bucketName, objName, srcPath, conds)
    }

    /**
     * Retrieve binary object from S3 store.
     * @param objName - The ID of the binary resource.
     */
    public async getBinary(objName: string): Promise<ObjectFromStore> {
        const object: Promise<Readable> = this.minioClient.getObject(this.bucketName, objName);
        return {
            createStream: (): Promise<Readable> => {
                return object;
            },
            headers: async (): Promise<OutgoingHttpHeaders> => {
                const stat: Minio.BucketItemStat = await this.minioClient.statObject(this.bucketName, objName);
                return {
                    "Content-Type": stat.metaData["content-type"],
                    "Content-Encoding": stat.metaData["content-encoding"],
                    "Cache-Control": stat.metaData["cache-control"],
                    "Content-Length": stat.size,
                };
            }
        };
    }

    /**
     * Returns a list with all timestamps corresponding to a revision of the given resource.
     * @param objName - Name of the relevant resource.
     */
    public async getRevisionList(objName: string): Promise<string[]> {
        const list: string[] = [];
        const stream = this.minioClient.listObjectsV2(this.bucketName, objName);
        return new Promise(function(resolve, reject) {
            stream.on('data', (obj) => list.push(obj.name.substring(obj.name.lastIndexOf('.') + 1)));
            stream.on('error', (error) => reject(error));
            stream.on('end', () => resolve(list));
        });
    }
}
export default BinaryDao;
