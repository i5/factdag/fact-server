import {Request, Response} from "express";
import * as CONST from "@shared/constants";
import * as STATUS from "http-status-codes";
import {
    createURIM,
    datetimeTotimestamp,
    fullUrl,
    fullUrlObj,
    isDatetimeInFuture,
    serializeTimemap, timestampToDatetime
} from "@shared/functions";
import logger from "@shared/Logger";
import BinaryDao from '@daos/Memento/BinaryDao';
import {UrlObject} from "url";
import * as Url from "url";

class BinaryMementoHandler {
    private binaryDao = new BinaryDao(process.env.S3_DEFAULT_BUCKET || "default_bucket");

    /**
     * Retrieve a binary from the S3 store.
     * @param req - Express Request object.
     * @param res - Express Response object.
     */
    async getBinary(req: Request, res: Response): Promise<Response> {
        const mementoHeader = req.header(CONST.HEADER.ACCEPT_DATETIME);
        if (mementoHeader) {
            if (isDatetimeInFuture(mementoHeader)) {
                return res.status(STATUS.NOT_ACCEPTABLE).send();
            }
        }
        const url = fullUrlObj(req);
        let query = undefined;
        let exactMatch = false;
        if (req.query.version !== undefined && typeof req.query.version === 'string') {
            query = req.query.version;
            exactMatch = true;
        } else if (mementoHeader) {
            query = datetimeTotimestamp(mementoHeader);
            exactMatch = false;
        }
        let binary;
        try {
            let mementoDatetime;
            if (exactMatch && query) {
                mementoDatetime = query;
                binary = await this.binaryDao.getBinary(BinaryMementoHandler.uriToFilePath(url, query));
            } else {
                const bestFittingRevisionID =  await this.determineSuitableRevision(url, query);
                binary = await this.binaryDao.getBinary(BinaryMementoHandler.uriToFilePath(url, bestFittingRevisionID));
                mementoDatetime = bestFittingRevisionID;
            }
            res.set(CONST.HEADER.MEMENTO_DATETIME, timestampToDatetime(mementoDatetime))
        } catch (e) {
            console.debug(e);
            return res.status(STATUS.NOT_FOUND).send();
        }
        let stream;
        try {
            stream = await binary.createStream();
            const headers = await binary.headers();
            Object.keys(headers).forEach((header) => {
                const value = headers[header];
                if (value !== undefined) {
                    res.setHeader(header, value);
                }
            });
        } catch (err) {
            if (err.code === "NotFound" || err.code === 'NoSuchKey') {
                return res
                    .status(STATUS.NOT_FOUND)
                    .send();
            } else {
                logger.error(err);
                return res.status(STATUS.INTERNAL_SERVER_ERROR).send("Unknown error");
            }
        }
        if (stream) {
            stream.on("error", (_e) => {
                logger.error(_e);
                return res.status(STATUS.INTERNAL_SERVER_ERROR).send("Unknown error");
            });
            return stream.pipe(res);
        }
        res.status(STATUS.OK);
        return res;
    }

    /**
     * Persists a binary to the S3 store as a new revision.
     * @param req - Express Request object.
     * @param res - Express Response object.
     */
    async writeBinary(req: Request, res: Response, revisionID: string): Promise<Response> {
        if (req.header(CONST.HEADER.CONTENT_TYPE) === undefined) {
            res.status(STATUS.BAD_REQUEST).send("Content-Type and Content-Length are required");
            return res;
        }
        let contentLength = req.header(CONST.HEADER.CONTENT_LENGTH);
        if (req.header(CONST.HEADER.CONTENT_LENGTH) === undefined) {
            contentLength = req.body.length;
        }
        const slug = req.header(CONST.HEADER.SLUG);
        const url = fullUrlObj(req);
        if (slug) {
            if (url.pathname && url.pathname.slice(-1) !== '/') {
                url.pathname += "/";
            }
            url.pathname += slug;
        }
        url.pathname = url.pathname?.replace(/\/$/, "");
        try {
            revisionID+="-0"
            const objName = BinaryMementoHandler.uriToFilePath(url, revisionID); 
            await this.binaryDao.writeBinary(req.body, objName, req.header(CONST.HEADER.CONTENT_TYPE) as string, req.header(CONST.HEADER.CONTENT_LENGTH) as string);
            res.set(CONST.HEADER.MEMENTO_DATETIME, timestampToDatetime(revisionID));
            res.set(CONST.HEADER.LOCATION, createURIM(Url.format(url), revisionID));
            res.status(STATUS.CREATED).send();
        } catch (e) {
            logger.error(e);
            res.status(STATUS.INTERNAL_SERVER_ERROR).send("Unknown error");
        }
        return res;
    }

    /**
     * Copies an existing S3 Object as a new revision.
     * @param req - Express Request object.
     * @param res - Express Response object.
     */
     async copyBinary(req: Request, res: Response, revisionID: string): Promise<Response> {
        const slug = req.header(CONST.HEADER.SLUG);
        const url = fullUrlObj(req);
        if (slug) {
            if (url.pathname && url.pathname.slice(-1) !== '/') {
                url.pathname += "/";
            }
            url.pathname += slug;
        }
        url.pathname = url.pathname?.replace(/\/$/, "");
        try {
            revisionID+="-0"
            const objName = BinaryMementoHandler.uriToFilePath(url, revisionID); 
            const bestFittingRevisionID =  await this.determineSuitableRevision(url, datetimeTotimestamp(Date.now().toString()));
            const srcObj = BinaryMementoHandler.uriToFilePath(url,bestFittingRevisionID)
            await this.binaryDao.copyBinary(objName, srcObj);
            res.set(CONST.HEADER.MEMENTO_DATETIME, timestampToDatetime(revisionID));
            res.set(CONST.HEADER.LOCATION, createURIM(Url.format(url), revisionID));
            res.status(STATUS.CREATED).send();
        } catch (e) {
            logger.error(e);
            res.status(STATUS.INTERNAL_SERVER_ERROR).send("Unknown error");
        }
        return res;
    }


    /**
     *  Constructs a Memento TimeMap for a given request.
     * @param req - Express Request object.
     * @param res - Express Response object.
     */
    async getTimeMap(req: Request, res: Response): Promise<Response>{
        const page = req.query.page;
        const range = req.header('accept-memento-range');
        let format = req.header('Accept');
        if (format === '*/*') {
            format = 'application/link-format';
        }
        if (format !== 'application/json' && format !== 'application/link-format' && format !== 'text/turtle') {
            return res.status(STATUS.NOT_ACCEPTABLE).send();
        }
        if (page && typeof page === 'string') {
            res.status(STATUS.NOT_IMPLEMENTED).send();
            // TODO Implement paged TimeMaps for binarys
        } else if (range !== undefined) {
            res.status(STATUS.NOT_IMPLEMENTED).send();
            // TODO Implement Ranged TimeMaps for binarys
        } else {
            const timestamps = await this.binaryDao.getRevisionList(BinaryMementoHandler.uriToFilePath(fullUrlObj(req)));
            const timemap = serializeTimemap(fullUrl(req), format, timestamps);
            res.set('content-type', format);
            res.status(STATUS.OK).send(timemap);
        }
        return res;
    }

    // TODO very inefficient approach. Consider using versioned Buckets, once available in JS-Client
    /**
     * Determines the revisionID that fits best to the requested timestamp.
     * If an exactly matching timestamp is found, it is returned. In that case, the function returns requestedTimestamp.
     * Otherwise, the largest timestamp (existing for a revision) that is smaller then the requested revisionID is determined.
     * If no requestedTimestamp is given, the largest timestamp is returned.
     * @param url - HTTP location of the relevant resource.
     * @param requestedTimestamp - RevisionID to be negotiated for the given resource.
     */
    private async determineSuitableRevision(url: UrlObject, requestedTimestamp?: string): Promise<string> {
        const list = await this.binaryDao.getRevisionList(BinaryMementoHandler.uriToFilePath(url));
        let element = "";
        list.forEach((el) => {
            if (el > element && (!requestedTimestamp || requestedTimestamp >= el)) {
                element = el;
            }
        });
        return element;
    }

    /**
     * Constructs the filename for a S3 object from the URL and a timestamp.
     * File names are constructed as follows.
     * Assuming a URL of the format:
     * ```
     * http://company.tdl/path/file.txt
     * ```
     * The S3 object is created at:
     * ```
     * bucketName/comany.tld/path/file.txt.timestamp
     * ```
     * @param url - Location of the resource.
     * @param revisionID - Optional Timestamp. If not given, only the prefix without the revisionID is returned.
     */
    private static uriToFilePath(url: UrlObject, revisionID?: string): string {
        let result = "" + url.host + url.pathname;
        if (revisionID) {
            result +=  "." + revisionID;
        }
        result = result.replace("/\:/g", "\.");
        return  result;
    }
}
export default BinaryMementoHandler;
