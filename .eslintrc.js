module.exports = {
  'env': {
    'browser': false,
    'es6': false,
    'node': true,
  },
  'extends': [
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'project': "./tsconfig.json",
    'tsconfigRootDir': __dirname,
    'ecmaVersion': 2020,
    'sourceType': 'module',
  },
  'plugins': [
    '@typescript-eslint',
    "eslint-plugin-tsdoc",
  ],
  'rules': {
    '@typescript-eslint/explicit-function-return-type': "warn",
    "tsdoc/syntax": "warn",
    'keyword-spacing': 'warn',
    'semi': "warn",
    'brace-style': "warn",
   // 'interface-name-prefix': ["warn", { "prefixWithI": "always", "allowUnderscorePrefix": true }],
  },
};
