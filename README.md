# Memento 2.0-Server
Prototypcial implementation of a REST server supporting the proposed, extended Memento-Protocol.
Please note, this is not an implementation intended for production use, but rather a working demonstration of the proposed protocol extensions.

## Features
This implementation features the following  Memento protocol extensions
* RFC3339 time format for `memento-datetime` and `accept-datetime` headers
* Returns the assigned `memento-datetime` as a response header for `PUT` and `POST` requests
* Accepts `PUT` requests with a `memento-datetime` header at an archiving endpoint, to allow the storage of Mementos created at another `URI-R` 
* Accepts `memento-datetime-range` requests to TimeMaps and returns only the requested range of Mementos.

as well as the known features of a Memento protocol implementation that combines TimeGates, Original Resources and Memento storage on the same server.
* Returns the current representation of a `URI-R` via HTTP `GET` as well as headers pointing to the TimeMap and TimeGate for that `URI-R`
* Returns a requested TimeMap listing the `URI-M`s associated with a `URI-R`, either completely or paged.
* Returns a Memento instead of the current representation for a `URI-R`, if the `accept-datetime` header is set in a `GET` request.
* Returns a Memento for direct request to a `URI-M`.

## Dependencies
The server implementation is written in Typescript for [Node](https://nodejs.org/en/) `v14` and uses [Express](https://expressjs.com/) `v4.17` as the web framework to handle RESTful requests.
[Redis](https://redis.io/) `v6` is used as an in-memory backend to store Mementos. The Node server interfaces with the Redis backend using [NodeRedis](https://github.com/NodeRedis/node-redis).

## How it works
The node server accepts HTTP requests and handles them using Express. Mementos are directly stored in the Redis backend using [Redis Streams](https://redis.io/topics/streams-intro).
Corresponding information on the used data structures, as well as time and space complexity, can be found in the respective Redis documentations.
The different operations are implemented as follows:

### Creating Resources
`PUT` requests may create new resources at any path on the server.
The server stores Mementos to a specific resource as a [Redis Stream](https://redis.io/topics/streams-intro).
Additionally a [List](https://redis.io/topics/data-types) of timestamps is used for each resource to be able to quickly assemble a TimeMap, if requested.
Redis returns a Timestamp if an element is added to a Stream. This Timestamp is formatted in RFC3339 and returned as a `memento-datetime` header with the response to the request.
HTTP `DELETE` requests to an existing resource add a Tombstone element to the Redis Stream and do not change the List managing the TimeMap.

### Retrieving Resources
For simple `GET` requests to resources, the last element of the Redis Stream is retrieved and returned. 
If an `accept-datetime` header is present, the associated Memento is retrieved from the Stream via the provided Timestamp.
If a simple `GET` request is directed to a deleted resource, the tombstone stored in Redis is interpreted, and the status code `410 Gone` is returned. However, Mementos may still be retrieved via `accept-datetime` requests to that resource.

### Retrieving TimeMaps
If a TimeMap is requested, it is assembled from the information in the Redis List, which only contains the Timestamps for a resource. 
The correct range of the List is requested from Redis using [lrange](https://redis.io/commands/lrange) if a paged TimeMap, or a TimeMap range is requested.

## Documentation
The [TypeDoc Documentation](https://i5.pages.rwth-aachen.de/factdag/fact-server)
documents the functions and classes of the FactServer.
